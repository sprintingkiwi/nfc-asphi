import zbar
image = read_image_into_numpy_array(...) # whatever function you use to read an image file into a numpy array
scanner = zbar.Scanner()
results = scanner.scan(image)
for result in results:
    print(result.type, result.data, result.quality, result.position)
